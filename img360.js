/**
 * Created by air on 01.05.15.
 */

function img360 (parentDiv){
    this.parentDiv = parentDiv; // Хранение блока родителя слайдера
    this.clicked = false; // Хранение состояния нажатия мышки
    this.currentActiveImage = 0; // Хранение индекса активной картинки
    this.yPosition =0; // Хранение координат мышки
    this.speedNum = 10; // Чем больше индекс, тем медленее скорость прокрутки TODO привязать к количеству слайдо
    this.loadingState = 0;

    // As browser parsed DOM, we can start work html elements.
    document.addEventListener('DOMContentLoaded', this.init.bind(this));

}

img360.prototype.init = function(){

    // Родительский контейнер
    this.parentDiv = document.querySelector(this.parentDiv);

    // Список картинок
    this.images = this.parentDiv.children;

    // Зачатки процесса загрузки
    var one_loading_punkt = Math.floor(100 / this.images.length);
    for (var i = 0, j = this.images.length; i < j; i++){
        this.images[i].addEventListener('load', function(){
            this.loadingScreen.innerText += '1 ';
        }.bind(this))
    }

    // Необходимо показать экран загрузки, показывает сообщение пока все картинки внутри
    // блока не загрузятся.
    this.showLoadingScreen();

    // Основные события
    this.parentDiv.addEventListener('mousedown', this.mousedown.bind(this)); // Нажата кнопка мышки, останавливаем автопрокрутку
    this.parentDiv.addEventListener('mouseup', this.mouseup.bind(this)); // Кнопка мыши отжата, значит прокрутку необходимо остановить
    this.parentDiv.addEventListener('mouseleave', this.mouseleave.bind(this)); // Как только курсор покидает контейнер, начинаем автопрокрутку
    //this.parentDiv.addEventListener('mouseenter', this.mousedown.bind(this));
    this.parentDiv.addEventListener('mousemove', this.move.bind(this), false);
    this.parentDiv.addEventListener('touchstart', this.mouseup.bind(this));
    this.parentDiv.addEventListener('touchmove', this.move.bind(this), false);

    // Защита от перетаскивания картинок
    this.parentDiv.addEventListener('dragstart', function(e){e.preventDefault(); return false});

    // Как только элементы загрузятся пользователю, можно скрыть экран загрузки.
    window.addEventListener('load', this.hideLoadingScreen.bind(this));
};

img360.prototype.startAutoPlay = function(){
    this.autoplay = setInterval(function () {
        this.changeImage('right');
    }.bind(this), 250);
}

img360.prototype.stoptAutoPlay = function(){
    this.stoptAutoPlay();
}

// Экран загрузки - это absolute блок который добавляется в подвал каждого img360 блока.
img360.prototype.showLoadingScreen = function(){
    this.loadingScreen = document.createElement('div');
    this.loadingScreen.className = 'loading';
    this.loadingScreen.innerText = 'Loading ' + this.images.length;
    this.parentDiv.appendChild(this.loadingScreen)
};

img360.prototype.hideLoadingScreen = function(){
    this.loadingScreen.remove();
    this.startAutoPlay();
};

img360.prototype.mousedown = function(){
    clearInterval(this.autoplay);
    this.clicked = true;
};

img360.prototype.mouseup = function(){
    this.clicked = false;
};

img360.prototype.mouseleave = function () {
    this.clicked = false;
    this.autoplay = setInterval(function () {
        this.changeImage('right');
    }.bind(this), 250);
}

img360.prototype.move = function() {
    if (this.clicked == true) {
        if (this.yPosition == 0) {
            this.yPosition = arguments[0].clientX;
        } else if (this.yPosition - 10  > arguments[0].clientX) {
            this.changeImage('left');
            this.yPosition = arguments[0].clientX;
        } else if (this.yPosition + 10 < arguments[0].clientX) {
            this.changeImage('right')
            this.yPosition = arguments[0].clientX;
        }
    }
}

img360.prototype.changeImage = function(direction){
    this.images[this.currentActiveImage].style.zIndex = '1';

    if (direction == 'left') {
        if (this.currentActiveImage == 0) this.currentActiveImage = this.images.length - 1;
        else this.currentActiveImage = this.currentActiveImage - 1;
    } else {
        if (this.currentActiveImage == this.images.length - 1) this.currentActiveImage = 0;
        else this.currentActiveImage +=1;
    }

    this.images[this.currentActiveImage].style.zIndex = '2';
}

// Init one shower
var img3601 = new img360('.img360')

// TODO Add array to constructor not one element