'use strict';

var _createClass = (function () {
    function defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
            var descriptor = props[i];descriptor.enumerable = descriptor.enumerable || false;descriptor.configurable = true;if ('value' in descriptor) descriptor.writable = true;Object.defineProperty(target, descriptor.key, descriptor);
        }
    }return function (Constructor, protoProps, staticProps) {
        if (protoProps) defineProperties(Constructor.prototype, protoProps);if (staticProps) defineProperties(Constructor, staticProps);return Constructor;
    };
})();

function _classCallCheck(instance, Constructor) {
    if (!(instance instanceof Constructor)) {
        throw new TypeError('Cannot call a class as a function');
    }
}

var img360 = (function () {
    function img360(imagesSelector) {
        var speedIndex = arguments[1] === undefined ? 10 : arguments[1];

        _classCallCheck(this, img360);

        this.imagesSelector = imagesSelector; // Селектор для выбора блоков с картинками, тип строка;
        this.speedIndex = speedIndex; // Индекс скорости прокрутки, чем больше индекс тем больше скорость, чем больше картинок тем больше должна быть скоорсть;
        document.addEventListener('DOMContentLoaded', this.init.bind(this)); // Необходимо дождаться загрузки дом-дерева, чтобы понять количество картинок внутри каждого блока.
    }

    _createClass(img360, [{
        key: 'init',
        value: function init() {
            this.imageBlocks = document.querySelectorAll(this.imagesSelector);
            if (this.imageBlocks.length === 0) return console.log('Нет картинок на странице.');
            // Показать экран загрузки для каждого блока
            for (var i = 0; i < this.imageBlocks.length; i++) {
                this.showLoadingScreen(this.imageBlocks[i]);
                // Добавление к каждому imageBlock дополнительного div
            }
            // Установить каждому элементу счётчик необходимых элементов для загрузки
            for (var i = 0; i < this.imageBlocks.length; i++) {
                // В свойство imagesNeedToLoad записать количество картинок внутри.
                this.imageBlocks[i].imagesNeedToLoad = this.imageBlocks[i].children.length;
                // навесить каждому изображению евент на загрузку и вычитать из родительского блока по 1,если
                // ImagesNeedToLoad == 0, убрать экран загрузки.
                for (var counter = 0; counter < this.imageBlocks[i].imagesNeedToLoad; counter++) {
                    this.imageBlocks[i].children[counter].addEventListener('load', this.imageLoaded(i).bind(this));
                };
            }
        }
    }, {
        key: 'imageLoaded',
        value: function imageLoaded(i) {
            return function () {
                //console.log('Для блока ' + i + "осталось загрузить " + this.imageBlocks[i].imagesNeedToLoad + "картинок");
                this.imageBlocks[i].imagesNeedToLoad -= 1;
                if (this.imageBlocks[i].imagesNeedToLoad == 1) this.hideLoadingScreen(i);

                this.imageBlocks[i].addEventListener('mousedown', this.mousedown); // Нажата кнопка мышки, останавливаем автопрокрутку
                this.imageBlocks[i].addEventListener('mouseup', this.mouseup); // Кнопка мыши отжата, значит прокрутку необходимо остановить
                this.imageBlocks[i].addEventListener('mousemove', this.move, false);

                // Защита от перетаскивания картинок
                this.imageBlocks[i].addEventListener('dragstart', function (e) {
                    e.preventDefault();return false;
                });
            };
        }
    }, {
        key: 'showLoadingScreen',
        value: function showLoadingScreen(divContainer) {
            divContainer.loadingScreen = document.createElement('div');
            divContainer.loadingScreen.className = 'loading';
            divContainer.loadingScreen.innerText = 'Loading ';
            divContainer.appendChild(divContainer.loadingScreen);
        }
    }, {
        key: 'hideLoadingScreen',
        value: function hideLoadingScreen(i) {
            this.imageBlocks[i].loadingScreen.remove();
        }
    }, {
        key: 'mousedown',
        value: function mousedown() {
            this.clicked = true;
            console.log('mousedown');
        }
    }, {
        key: 'mouseup',
        value: function mouseup() {
            this.clicked = false;
        }
    }, {
        key: 'changeImage',
        value: function changeImage(divContainer, direction) {
            console.log('Image changed;');
            this.images[this.currentActiveImage].style.zIndex = '1';

            if (direction == 'left') {
                if (this.currentActiveImage == 0) this.currentActiveImage = this.images.length - 1;else this.currentActiveImage = this.currentActiveImage - 1;
            } else {
                if (this.currentActiveImage == this.images.length - 1) this.currentActiveImage = 0;else this.currentActiveImage += 1;
            }

            this.images[this.currentActiveImage].style.zIndex = '2';
        }
    }, {
        key: 'move',
        value: function move() {
            if (this.clicked == true) {};
            return function () {
                this.changeImage();
            }
            //if (imageBlock.clicked == true) {
            //    if (imageBlock.xPosition == 'undefined') {
            //        imageBlock.xPosition = arguments[0].clientX;
            //    } else if (imageBlock.xPosition - 10  > arguments[0].clientX) {
            //        this.changeImage(imageBlock, 'left');
            //        imageBlock.xPosition = arguments[0].clientX;
            //    } else if (imageBlock.yPosition + 10 < arguments[0].clientX) {
            //        this.changeImage(imageBlock, 'right')
            //        imageBlock.xPosition = arguments[0].clientX;
            //    }
            //}
            ;
        }
    }, {
        key: 'startAutoPlay',
        value: function startAutoPlay() {
            this.autoplay = setInterval((function () {
                this.changeImage('right');
            }).bind(this), 250);
        }
    }]);

    return img360;
})();

var img3601 = new img360('.img360', 10);

//# sourceMappingURL=img360_bulk_compiled-compiled.js.map